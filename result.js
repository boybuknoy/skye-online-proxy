import AWS from "aws-sdk";
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const https = require('https');
const crypto = require('crypto');
const rp = require('request-promise');
const warmer = require('lambda-warmer');
const strongSoap = require('strong-soap').soap;


export function main(event, context, callback) {

  warmer(event).then(isWarmer => {
    if (isWarmer) {
      console.log('result warmed!!!');  
      callback(null,'warmed')
    // else proceed with handler logic
    } else {
      const transactionId = event.queryStringParameters.transaction;
	    const merchantId = event.queryStringParameters.id;
	    const returnUrl = event.queryStringParameters.url;
      const callbackUrl = event.queryStringParameters.callback;
      const test = event.queryStringParameters.test;
      
      const headers = {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true
      };

      if (test == 'true'){
        var soapUrl = 'https://captureuat.onceonline.com.au/IPL_service/ipltransaction.asmx?wsdl';
        //var sopaUrl = 'https://capturcsp.flexicards.com.au/IPL_service/ipltransaction.asmx?wsdl';
      }else{
        var soapUrl = 'https://applications.flexicards.com.au/IPL_service/ipltransaction.asmx?wsdl';
      }

      const requestArgs = {TransactionID: transactionId, MerchantId: merchantId};
	    const options = {};      
  	  var soapResponse = getTransactionDetails(soapUrl,options,requestArgs);

      var applicationStatus = soapResponse.then(function(result) {
        console.log(result.Status);
        console.log(result.ReturnApprovedUrl)

        if (result.Status == 'ACCEPTED')
        {

          if (test == 'true'){
            var soapUrl = 'https://captureuat.onceonline.com.au/IPL_service/ipltransaction.asmx?wsdl';
          }else{
            var soapUrl = 'https://applications.flexicards.com.au/IPL_service/ipltransaction.asmx?wsdl';
          }
          var commitResponse = commitTransaction(soapUrl, options, requestArgs);

          var commitStatus = commitResponse.then(function(commitResult){
              console.log('commitResult:'+commitResult)
              if (commitResult)
              {
                const params = {
                  TableName: "skye-ifol-merchants",                    
                  Key: {
                      merchantId: merchantId
                  }
                };
      
                const soapResults = result;
                dynamoDb.get(params, (error, dynamoResult) => {
                  var timeStamp = new Date().toISOString();
                  var merchantInfo = dynamoResult.Item;            
                  var secretKey = merchantInfo.secretKey;
                  
                  var hash = crypto.createHmac('sha256',secretKey.toString()).update('x_account_id'+merchantInfo.merchantId+'x_amount'+soapResults.Amount+'x_currencyAUDx_gateway_reference'+transactionId+'x_messagex_reference'+soapResults.OrderNumber+'x_resultcompletedx_test'+test+'x_timestamp'+timeStamp+'x_transaction_typesale').digest('hex');
                 
                  const data = {
                    x_account_id: soapResults.MerchantId,
                    x_amount: soapResults.Amount,
                    x_currency: 'AUD',
                    x_gateway_reference: transactionId,
                    x_reference: soapResults.OrderNumber,
                    x_result: 'completed',
                    x_test: test,
                    x_timestamp: timeStamp,
                    x_message: '',
                    x_transaction_type: 'sale',
                    x_signature: hash
                  }
                  
                  const httpOptions = {
                    method: 'POST',
                    uri: callbackUrl,
                    body: data,
                    json: true // Automatically stringifies the body to JSON
                  }
      
                  rp(httpOptions)
                    .then(function (parsedBody) {
                      // POST succeeded...                      
                      var redirectReturnUrl = returnUrl+'?x_account_id='+soapResults.MerchantId+'&x_amount='+soapResults.Amount+'&x_currency=AUD&x_gateway_reference='+transactionId+'&x_message=&x_reference='+soapResults.OrderNumber+'&x_result=completed&x_test='+test+'&x_timestamp='+timeStamp+'&x_signature='+hash+'&x_transaction_type=sale'
                  
                      var response = {     
                        statusCode: 301,
                        headers: {
                          Location: redirectReturnUrl
                        }
                      };
      
                      callback(null,response);
                    })
                    .catch(function (err) {
                      // POST failed...
                      console.log(JSON.stringify(err));
                      var responseError = {      
                        statusCode: 301,
                        headers: {
                          Location: soapResults.ReturnDeclineUrl
                        }
                      };
                      callback(responseError);  
                    });
                  })
              }else{                
                var responseError = {      
                  statusCode: 301,
                  headers: {
                     Location: result.ReturnDeclineUrl
                  }
                };
                callback(responseError);
              }
            }, function(err) {
              console.log('error:'+err);
              var responseError = {      
          		  statusCode: 301,
          		  headers: {
           			  Location: result.ReturnDeclineUrl
          		  }
      		    };
              callback(responseError);
           })                    
          
        }else {
            console.log(soapResults)
            console.log('Result status not accepted:'+result.Status)
              var responseError = {      
                  statusCode: 301,
                  headers: {
                     Location: soapResults.ReturnDeclineUrl
                  }
              };
            callback(responseError);      		
        }
      }, function(err) {
            console.log(err);
            var responseError = {      
          		statusCode: 301,
          		headers: {
           			Location: soapResults.ReturnDeclineUrl
          		}
      		  };
          callback(responseError);
      });
    }

    })
	
    function getTransactionDetails(url, options, requestArgs){
      return new Promise(function(resolve, reject) {
          strongSoap.createClient(url, options, function(err, client) {
              client.GetIPLTransaction(requestArgs, function(err, result, envelope, soapHeader) {
                  console.log(client.lastRequest)
                  if (err)
                  {
                      reject(err);
                  }else{                      
                      resolve(result.GetIPLTransactionResult);
                  }
              });
          });
      })
    };

    function commitTransaction(url, options, requestArgs){
      return new Promise(function(resolve, reject) {
          strongSoap.createClient(url, options, function(err, client) {
              client.CommitIPLTransaction(requestArgs, function(err, result, envelope, soapHeader) {
                  console.log(client.lastRequest)
                  if (err)
                  {
                  	  console.log('Commit err:'+err);
                      reject(err);
                  }else{    
                      console.log('success commit:'+result.CommitIPLTransactionResult);
                      resolve(result.CommitIPLTransactionResult);
                  }
              });
          });
      })
    };
};