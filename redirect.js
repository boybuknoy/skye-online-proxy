import AWS from "aws-sdk";
const { parse } = require('querystring');
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const warmer = require('lambda-warmer');
const strongSoap = require('strong-soap').soap;

export function main(event, context, callback) {
  // Request body is passed in as a JSON encoded string in 'event.body'
  const data = event.body;
 
    warmer(event).then(isWarmer => {
        if (isWarmer) {
            console.log('redirect warmed!');  
            callback(null,'warmed')
        // else proceed with handler logic
        } else {
            var formRequest = parse(data);
            console.log(formRequest);  

            const params = {
                TableName: "skye-ifol-merchants",                    
                Key: {
                    merchantId: formRequest['x_account_id']
                }
            };
  
            dynamoDb.get(params, (error, result) => {
                if (error) {
                    console.log(error);
                    var responseError = {      
                        statusCode: 301,
                        headers: {
                            Location: formRequest.x_url_cancel
                        }
                    }
                    callback(null, responseError);
                }else{
                    console.log(result.Item)
                    var streetBillingFormatted = parseStreetAddress(formRequest.x_customer_billing_address1);
                    var streetShippingFormatted = parseStreetAddress(formRequest.x_customer_shipping_address1);                   
                    var requestArgs = buildRequestArgs(result.Item, formRequest, streetBillingFormatted, streetShippingFormatted)
                    console.log(JSON.stringify(requestArgs));
                    const headers = {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": true
                    };
                    if (formRequest['x_test'].toString() == 'true'){                        
                        var soapUrl = 'https://captureuat.onceonline.com.au/IPL_service/ipltransaction.asmx?wsdl';
                        //var soapUrl = 'https://capturecsp.flexicards.com.au/IPL_service/ipltransaction.asmx?wsdl';
                        var redirectUrl = 'https://cxskyeuat.flexicards.com.au/PromotionSelector?seller=';
                        //var redirectUrl = 'https://cxskyecsp.flexicards.com.au/PromotionSelector?seller=';
                    }else{
                        var soapUrl = 'https://applications.flexicards.com.au/IPL_service/ipltransaction.asmx?wsdl';
                        var redirectUrl = 'https://apply.flexicards.com.au/PromotionSelector?seller=';
                    }

                    var options = {};
                    var transactionId = '';

                    var soapResponse = getTransactionId(soapUrl,options,requestArgs, transactionId);
  
                    soapResponse.then(function(result) {
                        console.log('result:'+result);
                        console.log(soapUrl)
                        console.log(redirectUrl)
                        transactionId = result;
                        console.log('transactionId:'+transactionId);
                        var response = {      
                            statusCode: 301,
                            headers: {
                                Location: redirectUrl+formRequest.x_account_id+'&ifol=True&transactionId='+transactionId
                            } 
                        };
                        callback(null, response);
                    }, function(err) {
                        console.log(err);
                        var responseError = {      
                            statusCode: 301,
                            headers: {
                                Location: formRequest.x_url_cancel
                            }
                        };
                        callback(null, responseError);
                    });
                }            
            }); 
        }
    })  
    
    function getTransactionId(url, options, requestArgs, transactionId){
      return new Promise(function(resolve, reject) {
          strongSoap.createClient(url, options, function(err, client) {
              client.BeginIPLTransaction(requestArgs, function(err, result, envelope, soapHeader) {
                  console.log(client.lastRequest)
                  if (err)
                  {
                      reject(err);
                  }else{
                      transactionId = JSON.stringify(result.BeginIPLTransactionResult).replace(/"/g,'');
                      resolve(JSON.stringify(result.BeginIPLTransactionResult).replace(/"/g,''));
                  }
              }, {timeout: 10000});
          });
      })
   };

   function buildRequestArgs(merchantInfo, formRequest, streetBillingFormatted, streetShippingFormatted){                    
        var customerPhone = formRequest.x_customer_phone
        if (formRequest.x_test == 'true')
        {
            var middlename = 'TWOPPPPPPPPPPPPPP'
        }else{
            var middlename = ''
        }
        var requestArgs = { TransactionInformation: 
            { MerchantId: formRequest.x_account_id, 
            OperatorId: merchantInfo.operatorId, 
            Password: merchantInfo.operatorPwd, 
            EncPassword: '', 
            Campaign: '', 
            Offer: merchantInfo.offerCode, 
            CreditProduct: 'MyBuy_Online', 
            NoApps: '', 
            OrderNumber: formRequest.x_reference, 
            ApplicationId: '', 
            Description: '', 
            Amount: formRequest.x_amount, 
            ExistingCustomer: 0, 
            Title: '',
            FirstName: formRequest.x_customer_first_name,
            MiddleName: middlename,
            Surname: formRequest.x_customer_last_name,
            Gender: '',
            BillingAddress: {AddressType: 'Residential', UnitNumber: '', StreetNumber: streetBillingFormatted.streetNo, StreetName: streetBillingFormatted.streetName, StreetType: streetBillingFormatted.streetType, Suburb: formRequest.x_customer_billing_city, City: formRequest.x_customer_billing_city, State: formRequest.x_customer_billing_state, Postcode: formRequest.x_customer_billing_zip, DPID: ''},
            DeliveryAddress: {AddressType: 'Residential', UnitNumber: '', StreetNumber: streetShippingFormatted.streetNo, StreetName: streetShippingFormatted.streetName, StreetType: streetShippingFormatted.streetType, Suburb: formRequest.x_customer_shipping_city, City: formRequest.x_customer_shipping_city, State: formRequest.x_customer_shipping_state, Postcode: formRequest.x_customer_shipping_zip, DPID: ''},
            WorkPhoneArea: '',
            WorkPhoneNumber: '',
            HomePhoneArea: '',
            HomePhoneNumber: '',
            MobilePhoneNumber: customerPhone? customerPhone.replace('+61','0') : '0400000000',
            EmailAddress: formRequest.x_customer_email? formRequest.x_customer_email : '',
            Status: '',
            ReturnApprovedUrl: 'https://unn133l657.execute-api.ap-southeast-2.amazonaws.com/prod/result?id='+formRequest.x_account_id+'&url='+formRequest.x_url_complete+'&callback='+formRequest.x_url_callback+'&test='+formRequest.x_test+'&transaction=[TRANSACTIONID]',
            ReturnDeclineUrl: formRequest.x_url_cancel,
            ReturnWithdrawUrl: formRequest.x_url_cancel,
            ReturnReferUrl: formRequest.x_url_cancel,
            CustomerID: '',
            SuccessPurch: '',
            SuccessAmt: '',
            DateLastPurch: '',
            PayLastPurch: '',
            DateFirstPurch: '',
            AcctOpen: '',
            CCDets: '',
            CCDeclines: '',
            CCDeclineNum: '',
            DeliveryAddressVal: '',
            Fraud: '',
            EmailVal: '',
            MobileVal: '',
            PhoneVal: '',
            TransType: '',
            UserField1: '',
            UserField2: '',
            UserField3: '',
            UserField4: '',
            SMSCustLink: '',
            EmailCustLink: '',
            SMSCustTemplate: '',
            EmailCustTemplate: '',
            SMSDealerTemplate: '',
            EmailDealerTemplate: '',
            EmailDealerSubject: '',
            EmailCustSubject: '',
            DealerEmail: '',
            DealerSMS: '',
            CreditLimit: ''
          }, SecretKey: merchantInfo.secretKey};

        return requestArgs;
   }

   function parseStreetAddress(formRequest){
        var stringStreet = formRequest

        var arrStreet = stringStreet.split(' ');
        
        if (isNaN(arrStreet[0])) {
            var streetNo = '99';
            var streetName = arrStreet[0];
            var streetType = arrStreet[1];
        }else{
            var streetNo = arrStreet[0];
            var streetName = arrStreet[1];
            var streetType = arrStreet[2];
        }
        var formattedAddress = {
            streetNo: streetNo,
            streetName: streetName,
            streetType: streetType.charAt(0).toUpperCase() + streetType.slice(1)
        }
        return formattedAddress;
   }
}